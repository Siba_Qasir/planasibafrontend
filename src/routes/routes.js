import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import TableList from "@/pages/TableList.vue";
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Notifications from "@/pages/Notifications.vue";
import UpgradeToPRO from "@/pages/UpgradeToPRO.vue";

import Login from "@/pages/Login.vue";
import Companies from "@/pages/companies/Comapnies.vue";
import AddCompany from "@/pages/companies/AddCompany.vue";
import EditCompany from "@/pages/companies/EditCompany.vue";
import Employees from "@/pages/employees/Employees.vue";
import AddEmployee from "@/pages/employees/AddEmployee.vue";
import EditEmployee from "@/pages/employees/EditEmployee.vue";

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard,
        beforeEnter : guardMyroute,
      },
      {
        path: "user",
        name: "User Profile",
        component: UserProfile,
        beforeEnter : guardMyroute,
      },
      {
        path: "companies",
        name: "Companies List",
        component: Companies,
        beforeEnter : guardMyroute,
      },
      {
        path: "companies/add",
        name: "Add Company",
        component: AddCompany,
        beforeEnter : guardMyroute,
      },
      {
        path: "companies-edit/:id",
        name: "Edit Company",
        component: EditCompany,
        beforeEnter : guardMyroute,
      },
      {
        path: "employees",
        name: "Employees List",
        component: Employees,
        beforeEnter : guardMyroute,
      },
      {
        path: "employees/add",
        name: "Add Employee",
        component: AddEmployee,
        beforeEnter : guardMyroute,
      },
      {
        path: "employees-edit/:id",
        name: "Edit Employee",
        component: EditEmployee,
        beforeEnter : guardMyroute,
      },
      // {
      //   path: "table",
      //   name: "Table List",
      //   component: TableList,
      // },
      // {
      //   path: "typography",
      //   name: "Typography",
      //   component: Typography,
      // },
      // {
      //   path: "icons",
      //   name: "Icons",
      //   component: Icons,
      // },
      // {
      //   path: "maps",
      //   name: "Maps",
      //   meta: {
      //     hideFooter: true,
      //   },
      //   component: Maps,
      // },
      // {
      //   path: "notifications",
      //   name: "Notifications",
      //   component: Notifications,
      // },
      // {
      //   path: "upgrade",
      //   name: "Upgrade to PRO",
      //   component: UpgradeToPRO,
      // },
    ],
  },
];

function guardMyroute(to, from, next)
{
  var isAuthenticated= false;
  if(localStorage.getItem('api_token'))
    isAuthenticated = true;
  else
    isAuthenticated= false;
  if(isAuthenticated) 
  {
    next();
  } 
  else
  {
    next('/login');
  }
}

export default routes;
